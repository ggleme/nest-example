import { Injectable, OnModuleInit } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsIn,
  validateOrReject,
} from 'class-validator';
import { plainToClass, Type } from 'class-transformer';

@Injectable()
export class ConfigService implements OnModuleInit {
  private readonly filePath: string;
  private parsedVariables: any;
  private envConfig: EnvVariables;

  constructor(filePath: string) {
    this.filePath = filePath;
  }

  async onModuleInit() {
    try {
      this.parsedVariables = dotenv.parse(fs.readFileSync(this.filePath));
      const config = plainToClass(EnvVariables, this.parsedVariables);
      await validateOrReject(config);
      this.envConfig = config;
    } catch (error) {
      throw new Error(`Config validation error: ${error}`);
    }
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}

class EnvVariables {
  @IsNotEmpty()
  @IsString()
  @IsIn(['development', 'test', 'production', 'provision', 'ci'])
  NODE_ENV: string = 'development';

  @Type(() => Number)
  @IsNumber()
  PORT: number = 3000;
}
