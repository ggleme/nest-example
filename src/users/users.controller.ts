import { Controller, Get, Post, Body, Param, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './domain/dto/create-user.dto';
// import { User } from './user.decorator';
import { FindOneUserDto } from './domain/dto/find-one-user.dto';
import { InactivateUserDto } from './domain/dto/inactivate-user.dto';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { CreatedUserDto } from './domain/dto/created-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async findAll() {
    try {
      const users = await this.userService.findAll();
      return {
        total: users.length,
        users,
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: number) {
    try {
      return await this.userService.findOne(id);
    } catch (error) {
      throw new Error(error);
    }
  }

  @Post()
  @ApiCreatedResponse({ type: CreatedUserDto })
  async create(@Body() dto: CreateUserDto): Promise<CreatedUserDto> {
    try {
      return await this.userService.create(dto);
    } catch (error) {
      throw new Error(error);
    }
  }

  @Put(':id/activate')
  async activate(@Param('id') id: number) {
    try {
      await this.userService.activate(id);
      return { message: `user ${id} activated!` };
    } catch (error) {
      throw new Error(error);
    }
  }

  @Put(':id/inactivate')
  async inactivate(@Param('id') id: number) {
    try {
      await this.userService.inactivate(id);
      return { message: `user ${id} inactivated!` };
    } catch (error) {
      throw new Error(error);
    }
  }
}
