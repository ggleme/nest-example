import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 127, unique: true })
  email: string;

  @Column({ length: 31, unique: true })
  username: string;

  @Column({ length: 127 })
  password: string;

  @Column({ length: 63 })
  firstName: string;

  @Column({ length: 63 })
  lastName: string;

  @Column({ nullable: true })
  birthDate: Date;

  @Column()
  inactive: boolean;
}
