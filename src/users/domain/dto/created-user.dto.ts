import { IsNumber, IsString, IsEmail, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreatedUserDto {
  @IsNumber()
  @ApiModelProperty({
    description: `user's id`,
    type: Number,
    example: 1,
  })
  id: number;

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty({
    description: `user's unique identification`,
    type: String,
    example: 'gleme',
  })
  username: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @ApiModelProperty({
    description: `user's email`,
    type: String,
    example: 'example@domain.com',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty({
    description: `user's first name`,
    type: String,
    example: 'John',
  })
  firstName: string;

  @IsNotEmpty()
  @IsString()
  @ApiModelProperty({
    description: `user's last name`,
    type: String,
    example: 'Doe',
  })
  lastName: string;

  constructor({ id, username, email, firstName, lastName }) {
    this.id = id;
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
