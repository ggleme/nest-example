import { IsNumberString } from 'class-validator';

export class FindOneUserDto {
  @IsNumberString()
  readonly id: number;
}
