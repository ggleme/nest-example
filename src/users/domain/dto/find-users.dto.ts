import { IsNumberString, IsString, IsNotEmpty, IsEmail } from 'class-validator';

export class FindUsersDto {
  @IsNumberString()
  readonly id: number;

  @IsString({ message: 'invalid type string' })
  @IsNotEmpty()
  readonly username: string;

  // TODO: transform email with crypto
  @IsString({ message: 'invalid type string' })
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
