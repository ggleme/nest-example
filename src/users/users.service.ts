import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { get } from 'lodash';
import { CreateUserDto } from './domain/dto/create-user.dto';
import { FindOneUserDto } from './domain/dto/find-one-user.dto';
import { FindUsersDto } from './domain/dto/find-users.dto';
import { User } from './domain/entities/user.entity';
import { InactivateUserDto } from './domain/dto/inactivate-user.dto';
import { CreatedUserDto } from './domain/dto/created-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createDto: CreateUserDto): Promise<CreatedUserDto> {
    try {
      const user = await this.userRepository.save({
        ...createDto,
        inactive: false,
      });
      const dto = new CreatedUserDto({ ...user });
      return dto;
    } catch (error) {
      throw error;
    }
  }

  async inactivate(id: number): Promise<boolean> {
    try {
      const result = await this.userRepository.update(id, {
        inactive: true,
      });

      const wrong = get(result.raw, 'affectedRows', 0) === 1;
      if (wrong) {
        return true;
      } else {
        throw new Error('cannot change more than on record');
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  async activate(id: number): Promise<boolean> {
    try {
      const result = await this.userRepository.update(id, {
        inactive: false,
      });

      const wrong = get(result.raw, 'affectedRows', 0) === 1;
      if (wrong) {
        return true;
      } else {
        throw new Error('cannot change more than on record');
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  findOne(id: number): Promise<User> {
    try {
      return this.userRepository.findOneOrFail(id);
    } catch (error) {
      throw new Error(error);
    }
  }

  findAll(): Promise<FindUsersDto[]> {
    try {
      return this.userRepository.find();
    } catch (error) {
      throw new Error(error);
    }
  }
}
