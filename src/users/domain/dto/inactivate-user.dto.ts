import { IsNumberString } from 'class-validator';

export class InactivateUserDto {
  @IsNumberString()
  id: number;
}
