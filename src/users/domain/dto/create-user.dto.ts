import { IsString, IsEmail, Length, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @IsString({ message: 'invalid type' })
  @Length(4, 100)
  @IsNotEmpty()
  @IsEmail()
  @ApiModelProperty({
    description: `user's email`,
    required: true,
    type: String,
    example: 'example@domain.com',
  })
  readonly email: string;

  @IsString({ message: 'invalid type' })
  @Length(2, 127)
  @IsNotEmpty()
  @ApiModelProperty()
  readonly firstName: string;

  @IsString({ message: 'invalid type' })
  @Length(2, 127)
  @IsNotEmpty()
  @ApiModelProperty()
  readonly lastName: string;

  @IsString({ message: 'invalid type' })
  @Length(2, 17)
  @IsNotEmpty()
  @ApiModelProperty()
  readonly username: string;

  @IsString()
  @Length(8, 31)
  @IsNotEmpty()
  @ApiModelProperty()
  readonly password: string;
}
